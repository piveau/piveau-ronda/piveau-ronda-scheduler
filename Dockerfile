FROM openjdk:11-jre

ENV VERTICLE_NAME io.piveau.DataStore.Scheduler.MainVerticle
ENV VERTICLE_FILE Scheduler-1.0.0-fat.jar

ENV ENV prod
# Set the location of the verticles
ENV VERTICLE_HOME /usr/verticles

# Download Postgres JDBC Driver



# Copy your fat jar to the container
COPY target/$VERTICLE_FILE $VERTICLE_HOME/

# Launch the verticle
WORKDIR $VERTICLE_HOME
ENTRYPOINT ["sh", "-c"]
CMD ["exec java $JAVA_OPTS -jar $VERTICLE_FILE -classpath $VERTICLE_NAME"]
