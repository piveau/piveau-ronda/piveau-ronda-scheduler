# Scheduler

## Functionality
The datastore can process real-time streaming data as well as static datasets.
It uses a Vertx Cluster to run Connector Verticles to fetch real-time data sources and a Webserver Verticle for users to upload datasets.
The incoming real-time data gets forwarded through Apache Kafka, processed with Spark Streaming and saved to an HDFS Cluster.
The static datasets can be uploaded to HDFS directly. A Http and Websocket Server is running also in Vertx to provide the processed data.


## Prerequisites
* Docker (Production & Development)
* Docker-compose (Production & Development)
* Maven (Development)
* Java (Development)

## Build Instructions
To build and start the project execute the following command in a shell from the project folder.

1. Start Proprietary Projects by executing: "docker-compose up"
2. Build the project by executing: "mvn package"
3. Start the Scheduler app by executing: "mvn exec:java"

## Usage
The *jobExecutionContext* holds all parameters given for that job.
The easiest way to use them is to extract them as json. Also, a new Vertx instance
is created for that job and also needs to be after all computations are done.

Datasets and distributions uses the [Apache Commons StringSubstitutor](http://commons.apache.org/proper/commons-text/apidocs/org/apache/commons/text/StringSubstitutor.html) for replacing variables in the dataset/distribution templates.
Both datasets and distributions must be in Turtle format. Variables can be defined using `${variable}`. The according values are denoted without the special characters, using `variable`.
The following values are generated at runtime and are available for substitution:

| Value | Description |
| :---  | :---        |
| `datasetId` | Random UUID |
| `distributionId` | Random UUID |
| `datastream` | Name of the current datastream |
| `datastreamUrl` | Link to the upstream data |
| `timestamp` | Current dateTime formatted as [ISO_LOCAL_DATE_TIME](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html#ISO_LOCAL_DATE_TIME) |
| `day` | Day of current month |
| `month` | Month of current year |
| `year` | Current year |

A dataset making use of templating would lokk something like this:
```
@prefix schema: <http://schema.org/> .
@prefix dcatap: <http://data.europa.eu/r5r/> .
@prefix adms:  <http://www.w3.org/ns/adms#> .
@prefix spdx:  <https://spdx.org/rdf/terms/#> .
@prefix gsp:   <http://www.opengis.net/ont/geosparql#> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix org:   <http://www.w3.org/ns/org#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix hydra: <http://www.w3.org/ns/hydra/core#> .
@prefix dct:   <http://purl.org/dc/terms/> .
@prefix v:     <http://www.w3.org/2006/vcard/ns#> .
@prefix time:  <http://www.w3.org/2006/time#> .
@prefix dcat:  <http://www.w3.org/ns/dcat#> .
@prefix odrl:  <https://www.w3.org/TR/odrl-vocab/#> .
@prefix locn:  <http://www.w3.org/ns/locn#> .
@prefix prov:  <http://www.w3.org/ns/prov#> .
@prefix foaf:  <http://xmlns.com/foaf/0.1/> .
@prefix gmd:   <http://www.isotc211.org/2005/gmd#> .

<http://www.piveau.eu/set/distribution/${distributionId}>
        a                dcat:Distribution ;
        dct:description  "Echtzeit Daten für ${datastream}"@de ;
        dct:format       <http://publications.europa.eu/resource/authority/file-type/JSON> ;
        dct:license      <http://publications.europa.eu/resource/authority/licence/CC_BY> ;
        dct:title        "Echtzeit Daten für ${datastream}"@de ;
        dcat:accessURL   <${datastreamUrl}> .

<http://www.piveau.eu/set/data/${datasetId}>
        a                  dcat:Dataset ;
        dct:accessRights   [ a           dct:accessRights ;
                             rdfs:label  "public"
                           ] ;
        dct:description    "${description}"@de ;
        dct:issued         "${timestamp}"^^xsd:dateTime ;
        dct:modified       "${timestamp}"^^xsd:dateTime ;
        dct:publisher      [ a              foaf:Organization ;
                             foaf:homepage  "https://www.fokus.fraunhofer.de" ;
                             foaf:name      "Fraunhofer FOKUS"
                           ] ;
        dct:title          "Echtzeitdaten für ${datastream} vom ${day}.${month}.${year}"@de ;
        dcat:contactPoint  [ a           v:Organization ;
                             v:fn        "Fabian Kirstein" ;
                             v:hasEmail  "mailto:fabian.kirstein@fokus.fraunhofer.de"
                           ] ;
        dcat:distribution  <http://www.piveau.eu/set/distribution/${distributionId}> ;
        dcat:keyword       "${keyword1}" , "${keyword2}" ;
        dcat:theme         <http://publications.europa.eu/resource/authority/data-theme/REGI> , <http://publications.europa.eu/resource/authority/data-theme/ENER> ;
        foaf:page          <http://datastore.k2dev.fokus.fraunhofer.de/> .
```

The distribution would look something like this:

```
<http://www.piveau.eu/set/distribution/${distributionId}>
        a                dcat:Distribution ;
        dct:description  "Echtzeit Daten für ${datastream}"@de ;
        dct:format       <http://publications.europa.eu/resource/authority/file-type/JSON> ;
        dct:license      <http://publications.europa.eu/resource/authority/licence/CC_BY> ;
        dct:title        "Echtzeit Daten für ${datastream}"@de ;
        dcat:accessURL   <${datastreamUrl}> .
```



The resulting config would look something as follows. Note that for the distribution no custom replacements are configured, since the template entirely relies on the ones generated at runtime.
As such, the respective key can be omitted. 
```json
{
  "PiveauHubConnectorJob": {
    "hubUrl": "<Hub URL>",
    "hubApiKey": "<API Key>",
    "connectorUrl": "http://datastore-connector:8080",
    "externalConnectorUrl": "<External URL>",
    "dayRange": 3,
    "uploadSettings": {
      "OpenData.OpenWeather": {
        "frequencies": [
          "daily",
          "monthly"
        ],
        "replacements": {
          "title": "Wetter Wiesbaden",
          "distributionTitle": "Aggregierte Wetterdaten",
          "description": "Aggregierte Wetterdaten für Wiesbaden.",
          "keyword1": "Real-time",
          "keyword2": "Wetter"
        },
        "dataset": {
          "template": "@prefix schema: <http://schema.org/> .\n@prefix dcatap: <http://data.europa.eu/r5r/> .\n@prefix adms:  <http://www.w3.org/ns/adms#> .\n@prefix spdx:  <https://spdx.org/rdf/terms/#> .\n@prefix gsp:   <http://www.opengis.net/ont/geosparql#> .\n@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n@prefix org:   <http://www.w3.org/ns/org#> .\n@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n@prefix hydra: <http://www.w3.org/ns/hydra/core#> .\n@prefix dct:   <http://purl.org/dc/terms/> .\n@prefix v:     <http://www.w3.org/2006/vcard/ns#> .\n@prefix time:  <http://www.w3.org/2006/time#> .\n@prefix dcat:  <http://www.w3.org/ns/dcat#> .\n@prefix odrl:  <https://www.w3.org/TR/odrl-vocab/#> .\n@prefix locn:  <http://www.w3.org/ns/locn#> .\n@prefix prov:  <http://www.w3.org/ns/prov#> .\n@prefix foaf:  <http://xmlns.com/foaf/0.1/> .\n@prefix gmd:   <http://www.isotc211.org/2005/gmd#> .\n\n<http://www.piveau.eu/set/distribution/${distributionId}>\n        a                dcat:Distribution ;\n        dct:description  \"${distributionDescription}\"@de ;\n        dct:format       <http://publications.europa.eu/resource/authority/file-type/JSON> ;\n        dct:license      <http://publications.europa.eu/resource/authority/licence/CC_BY> ;\n        dct:title        \"${distributionTitle} für ${distributionDate}\"@de ;\n        dcat:accessURL   <${datastreamUrl}> .\n\n<http://www.piveau.eu/set/data/${datasetId}>\n        a                  dcat:Dataset ;\n        dct:accessRights   [ a           dct:accessRights ;\n                             rdfs:label  \"public\"\n                           ] ;\n        dct:description    \"${description}\"@de ;\n        dct:issued         \"${timestamp}\"^^xsd:dateTime ;\n        dct:modified        \"${timestamp}\"^^xsd:dateTime ;\n        dct:publisher      [ a              foaf:Organization ;\n                             foaf:homepage  \"https://www.fokus.fraunhofer.de\" ;\n                             foaf:name      \"Fraunhofer FOKUS\"\n                           ] ;\n        dct:title          \"${title} für ${datasetDate}\"@de ;\n        dcat:contactPoint  [ a           v:Organization ;\n                             v:fn        \"Fabian Kirstein\" ;\n                             v:hasEmail  \"fabian.kirstein@fokus.fraunhofer.de\"\n                           ] ;\n        dcat:distribution  <http://www.piveau.eu/set/distribution/${distributionId}> ;\n        dcat:keyword       \"${keyword1}\" , \"${keyword2}\" ;\n        dcat:theme         <http://publications.europa.eu/resource/authority/data-theme/REGI> , <http://publications.europa.eu/resource/authority/data-theme/ENER> ;\n        foaf:page          <http://datastore.k2dev.fokus.fraunhofer.de/> ."
        },
        "distribution": {
          "template": "@prefix schema: <http://schema.org/> .\n@prefix dcatap: <http://data.europa.eu/r5r/> .\n@prefix adms:  <http://www.w3.org/ns/adms#> .\n@prefix spdx:  <https://spdx.org/rdf/terms/#> .\n@prefix gsp:   <http://www.opengis.net/ont/geosparql#> .\n@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n@prefix org:   <http://www.w3.org/ns/org#> .\n@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n@prefix hydra: <http://www.w3.org/ns/hydra/core#> .\n@prefix dct:   <http://purl.org/dc/terms/> .\n@prefix v:     <http://www.w3.org/2006/vcard/ns#> .\n@prefix time:  <http://www.w3.org/2006/time#> .\n@prefix dcat:  <http://www.w3.org/ns/dcat#> .\n@prefix odrl:  <https://www.w3.org/TR/odrl-vocab/#> .\n@prefix locn:  <http://www.w3.org/ns/locn#> .\n@prefix prov:  <http://www.w3.org/ns/prov#> .\n@prefix foaf:  <http://xmlns.com/foaf/0.1/> .\n@prefix gmd:   <http://www.isotc211.org/2005/gmd#> .\n\n<http://www.piveau.eu/set/distribution/${distributionId}>\n        a                dcat:Distribution ;\n        dct:description  \"${distributionDescription}\"@de ;\n        dct:format       <http://publications.europa.eu/resource/authority/file-type/JSON> ;\n        dct:license      <http://publications.europa.eu/resource/authority/licence/CC_BY> ;\n        dct:title        \"${distributionTitle} für ${distributionDate}\"@de ;\n        dcat:accessURL   <${datastreamUrl}> ."
        }
      }
    }
  },
  "RestartSparkJob": {
    "hdfsHost": "namenode",
    "hdfsPort": 9870,
    "hdfsRootDirectory": "/Piveau/DataStore",
    "fileLimit": 100000
  },
  "PIVEAU_DATASTORE_SCHEDULER_SCHEDULE": {
    "PiveauHubConnectorJob": "0 0 0/4 ? * * *",
    "RestartSparkJob": "0 0 0 ? * MON"
  }
}
```

The **PIVEAU_DATASTORE_SCHEDULER_SCHEDULE** registers job for schedules.
The *key* is always the class name of the job and the value is the schedule in *cron* notation style.
In the example above the job is executed every 20 seconds.
Optionally parameters can be given to the job. In this case the key also is always the class name. The value is a json with the actual parameter data.
The content of the json can differ for each job depending on the implementation of the job itself.
