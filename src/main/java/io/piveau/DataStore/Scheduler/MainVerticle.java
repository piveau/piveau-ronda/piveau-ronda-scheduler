package io.piveau.DataStore.Scheduler;

import io.piveau.DataStore.Scheduler.verticles.SchedulerVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.piveau.DataStore.Scheduler.util.ApplicationConfig.*;

public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private JsonObject config;

    @Override
    public void start(Promise<Void> startPromise) {
        log.info("Starting Scheduler...");
        loadConfig()
                .compose(handler -> startScheduler())
                .onSuccess(success -> startPromise.complete())
                .onFailure(startPromise::fail);
    }

    private Future<Void> startScheduler(){
        log.info("Starting Scheduler...");
        Promise<String> started = Promise.promise();
        vertx.deployVerticle(SchedulerVerticle.class, new DeploymentOptions().setConfig(config).setWorker(true), started);
        return started.future().map(s -> null);
    }
    private Future<Void> loadConfig() {
        log.info("Loading config...");
        return Future.future(loadConfig -> {
            ConfigRetrieverOptions options = new ConfigRetrieverOptions()
                    .addStore(new ConfigStoreOptions()
                            .setType("file")
                            .setFormat("json")
                            .setConfig(new JsonObject().put("path", PATH_JSON_CONFIG))
                            .setOptional(true)
                    )
                    .addStore(new ConfigStoreOptions()
                            .setType("env")
                    );

            ConfigRetriever.create(vertx, options).getConfig(handler -> {
                if (handler.succeeded()) {
                    config = handler.result();
                    log.info("Using config" + config.encodePrettily());

                    if ( config.containsKey(ENV_SCHEDULE)) {
                        loadConfig.complete();
                    } else {
                        loadConfig.fail("Schedule missing in Config");
                    }
                } else {
                    loadConfig.fail("Failed to load config: " + handler.cause());
                }
            });
        });
    }
}


