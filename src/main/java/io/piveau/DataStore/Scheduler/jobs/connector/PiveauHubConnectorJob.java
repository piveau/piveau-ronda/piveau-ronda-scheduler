package io.piveau.DataStore.Scheduler.jobs.connector;


import io.piveau.DataStore.Scheduler.util.PiveauHubUploadUtils;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The Piveau Hub Connector Job contains the functionality to connect the API provides by the Connector module
 * to a Piveau instance
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class PiveauHubConnectorJob implements Job {

    private static final Logger log = Logger.getLogger(PiveauHubConnectorJob.class);

    private final Vertx vertx = Vertx.vertx();
    private PiveauHubUploadUtils piveauHubUploadUtils;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("Started " + PiveauHubConnectorJob.class.getCanonicalName() + " Job");

        JsonObject jobData = new JsonObject(jobExecutionContext.getMergedJobDataMap());
        String connectorUrl = jobData.getString("connectorUrl");
        String externalUrl = jobData.getString("externalConnectorUrl", connectorUrl);
        String hubUrl = jobData.getString("hubUrl");
        String hubAPIKey = jobData.getString("hubApiKey");
        int dayRange = jobData.getInteger("dayRange",2);


        JsonObject uploadSettings = jobData.getJsonObject("uploadSettings");

        piveauHubUploadUtils = new PiveauHubUploadUtils(vertx, connectorUrl,externalUrl, hubUrl, hubAPIKey, uploadSettings);
        Future<List<String>> datastreamsFuture = piveauHubUploadUtils.getAllDatastreams();

        LocalDate currentDate = LocalDate.now();

        handleDates(datastreamsFuture, currentDate, dayRange).onComplete(complete -> {
           log.info("Finished Job");
            piveauHubUploadUtils.close(Promise.promise());
            vertx.close();
        });
    }

    private Future<Void> handleDates(Future<List<String>> datastreamsFuture, LocalDate dateBase, Integer i){
        List<Future> uploadsFinished = new ArrayList<>();
        Promise<Void> dailyUploadPromise = Promise.promise();
        uploadsFinished.add(dailyUploadPromise.future());

        LocalDate currentDate = dateBase.minusDays(i);
        log.info("Handling Date: " + currentDate);
        // Daily Uploads
        datastreamsFuture
                .flatMap(datastreams -> {
                    List<Future> dailyUploads = datastreams.stream()
                            .map(datastream -> handleDailyUpload(datastream, currentDate))
                            .collect(Collectors.toList());

                    return CompositeFuture.all(dailyUploads);
                })
                .onComplete(result -> dailyUploadPromise.complete());

        // Monthly Uploads
        if (currentDate.getDayOfMonth() == 1) {
            Promise<Void> monthlyUploadPromise = Promise.promise();
            uploadsFinished.add(monthlyUploadPromise.future());
            LocalDate lastMonth = currentDate.minusMonths(1);

            datastreamsFuture
                    .flatMap(datastreams -> {
                        List<Future> dailyUploads = datastreams.stream()
                                .map(datastream -> handleMonthlyUpload(datastream, lastMonth))
                                .collect(Collectors.toList());

                        return CompositeFuture.all(dailyUploads);
                    })
                    .onComplete(complete -> monthlyUploadPromise.complete());
        }
        if(i > 0){
            return CompositeFuture.all(uploadsFinished).flatMap(aVoid -> handleDates(datastreamsFuture,dateBase, i-1));
        }
        return  CompositeFuture.all(uploadsFinished).map(aVoid -> null);
    }

    private Future<Void> handleDailyUpload(String datastream, LocalDate date) {
        return Future.future(dailyUpload ->
                piveauHubUploadUtils.registeredForDailyUpload(datastream)
                        .flatMap(registered -> registered
                                ? piveauHubUploadUtils.dailyWindowExistsInDataStore(datastream, date)
                                : Future.succeededFuture(false)
                        )
                        .flatMap(dataExists -> dataExists
                                ? piveauHubUploadUtils.doDailyUpload(datastream, date)
                                : Future.succeededFuture()
                        )
                        .setHandler(dailyUpload));
    }

    private Future<Void> handleMonthlyUpload(String datastream, LocalDate date) {
        return Future.future(monthlyUpload ->
                piveauHubUploadUtils.registeredForMonthlyUpload(datastream)
                        .flatMap(registered -> registered
                                ? piveauHubUploadUtils.monthlyWindowExistsInDataStore(datastream, date)
                                : Future.succeededFuture(false)
                        )
                        .flatMap(dataExists -> dataExists
                                ? piveauHubUploadUtils.doMonthlyUpload(datastream, date)
                                : Future.succeededFuture()
                        )
                        .setHandler(monthlyUpload));
    }
}
