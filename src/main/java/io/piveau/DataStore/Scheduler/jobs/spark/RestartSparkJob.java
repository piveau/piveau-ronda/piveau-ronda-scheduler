package io.piveau.DataStore.Scheduler.jobs.spark;


import io.piveau.DataStore.Commons.schema.DataFormat;
import io.piveau.DataStore.Commons.schema.HdfsStructure;
import io.piveau.DataStore.Commons.schema.KafkaTopics;
import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.log4j.Logger;
import org.quartz.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/** The Piveau Hub Connector Job contains the functionality to connect the API provides by the Connector module
 * to a Piveau instance */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RestartSparkJob implements Job {

    private static final Logger log = Logger.getLogger(RestartSparkJob.class);

    Vertx vertx = Vertx.vertx();
    WebClient webClient;

    JsonObject jobData;
    String hdfsHost;
    Integer hdfsPort;
    Integer fileLimit;
    HdfsStructure hdfsStructure;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("Started " + RestartSparkJob.class.getCanonicalName() + " Job");
        webClient = WebClient.create(vertx);

        this.jobData = new JsonObject(jobExecutionContext.getMergedJobDataMap());
        hdfsHost = this.jobData.getString("hdfsHost");
        hdfsPort = this.jobData.getInteger("hdfsPort");
        fileLimit = this.jobData.getInteger("fileLimit");
        String hdfsRootDirectory = this.jobData.getString("hdfsRootDirectory");

        hdfsStructure = new HdfsStructure(hdfsRootDirectory);

        List<Future> jobFinished = new LinkedList<>();

        Promise<HttpResponse<Buffer>> contentResponse = Promise.promise();
        webClient.get(hdfsPort,hdfsHost,"/webhdfs/v1" + hdfsStructure.rootDir)
                .addQueryParam("op", "GETCONTENTSUMMARY")
                .send(contentResponse);

        Future<JsonObject> jsonResponse = contentResponse.future().map(HttpResponse::bodyAsJsonObject);
        jsonResponse.onFailure(error -> log.error("Could not fulfil content Request", error));

        Future<Integer> fileCount = jsonResponse.map(json -> json.getJsonObject("ContentSummary").getInteger("fileCount"));

        Future<Void> finished = fileCount
                .flatMap(count -> count > fileLimit ? deleteCheckpoints(): Future.succeededFuture());

        jobFinished.add(finished);
        CompositeFuture.all(jobFinished).onComplete(complete -> vertx.close());
    }

    private Future<Void> deleteCheckpoints(){
        log.info("Deleting Checkpoints...");

        // Delete Spar Metadata
        Promise<HttpResponse<Buffer>> getOpenDataTopicResponse = Promise.promise();
        webClient.get(hdfsPort,hdfsHost,"/webhdfs/v1" + hdfsStructure.opendataDir)
                .addQueryParam("op", "LISTSTATUS")
                .send(getOpenDataTopicResponse);

        Future<List<Topic>> openDataTopics = getOpenDataTopicResponse.future()
                .map(HttpResponse::bodyAsJsonObject)
                .map(jsonObject -> jsonObject.getJsonObject("FileStatuses").getJsonArray("FileStatus"))
                .map(jsonObjects -> jsonObjects.stream().map(obj -> (JsonObject) obj).map(obj -> KafkaTopics.valueOf(obj.getString("pathSuffix"))))
                .map(stringStream -> stringStream.collect(Collectors.toList()));

        Promise<Void> deleteAllSparkMeta = Promise.promise();

        openDataTopics
                .map(topics -> topics.stream().filter(Objects::nonNull).peek(topic -> log.info(topic.topicName)).map(this::deleteSparkMeta))
                .flatMap(stream -> CompositeFuture.all(stream.collect(Collectors.toList())))
                .flatMap(compositeFuture -> compositeFuture.flatMap(aVoid -> Future.succeededFuture()))
        .onSuccess(aVoid -> deleteAllSparkMeta.complete())
        .onFailure(deleteAllSparkMeta::fail);

        ;

        // Delete Checkpoint Folder
        Promise<HttpResponse<Buffer>> deleteCheckpointResponse = Promise.promise();
        webClient.delete(hdfsPort,hdfsHost,"/webhdfs/v1" + hdfsStructure.checkpointDir)
                .addQueryParam("op", "DELETE")
                .addQueryParam("recursive", "true")
                .send(deleteCheckpointResponse);

        return CompositeFuture.all(deleteAllSparkMeta.future(), deleteCheckpointResponse.future())
                .onFailure(error -> log.error("Could not delete all Checkpoint Data", error))
                .onSuccess(aVoid -> log.info("Finished Deleting Checkpoint Data"))
                .flatMap(compositeFuture -> compositeFuture.flatMap(aVoid -> Future.succeededFuture()));
    }

    private Future<Void> deleteSparkMeta(Topic topic) {
        Key<?> key = topic.keys.iterator().next();

        List<String> sparkMetaDataPath = Arrays.stream(DataFormat.values())
                .map(format -> hdfsStructure.topicDirectory(topic, key, format) + "/_spark_metadata")
                .collect(Collectors.toList());

        List<Future> deleteAllSparMetadata = sparkMetaDataPath.stream().map(path -> {
            Promise<HttpResponse<Buffer>> deleteRespone = Promise.promise();
            webClient.delete(hdfsPort, hdfsHost, "/webhdfs/v1" + path)
                    .addQueryParam("op", "DELETE")
                    .addQueryParam("recursive", "true")
                    .send(deleteRespone);
            return (Future) deleteRespone.future();
        }).collect(Collectors.toList());

        return CompositeFuture.all(deleteAllSparMetadata)
                .onSuccess(aVoid -> log.info("Deleted SparkMetadata for " + topic.topicName))
                .onFailure(error -> log.error("Could not delete Sparkmetadata for " + topic.topicName, error))
                .flatMap(compositeFuture -> Future.succeededFuture());
    }
}
