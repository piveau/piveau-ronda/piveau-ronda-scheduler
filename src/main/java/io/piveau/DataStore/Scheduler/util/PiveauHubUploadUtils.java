package io.piveau.DataStore.Scheduler.util;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.ext.web.client.predicate.ResponsePredicateResult;
import io.vertx.ext.web.codec.BodyCodec;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

public class PiveauHubUploadUtils implements Closeable {

    private static final Logger log = LoggerFactory.getLogger(PiveauHubUploadUtils.class);

    private final String connectorUrl;
    private final String externalConnectorUrl;
    private final String hubUrl;
    private final String hubAPIKey;
    private final JsonObject uploadSettings;
    private final WebClient webClient;
    private final CircuitBreaker circuitBreaker;

    public PiveauHubUploadUtils(Vertx vertx, String connectorUrl,String externalConnectorUrl, String hubUrl, String hubAPIKey, JsonObject uploadSettings) {
        this.webClient = WebClient.create(vertx);
        this.circuitBreaker = CircuitBreaker.create(PiveauHubUploadUtils.class.getCanonicalName(), vertx);
        this.connectorUrl = connectorUrl;
        this.hubUrl = hubUrl;
        this.hubAPIKey = hubAPIKey;
        this.uploadSettings = uploadSettings;
        this.externalConnectorUrl = externalConnectorUrl;
    }

    // return future for easier processing of result
    public Future<Boolean> registeredForDailyUpload(String datastream) {
        return Future.succeededFuture(uploadSettings.containsKey(datastream)
                && uploadSettings.getJsonObject(datastream).getJsonArray("frequencies").contains("daily"));
    }

    // return future for easier processing of result
    public Future<Boolean> registeredForMonthlyUpload(String datastream) {
        return Future.succeededFuture(uploadSettings.containsKey(datastream)
                && uploadSettings.getJsonObject(datastream).getJsonArray("frequencies").contains("monthly"));
    }

    public Future<Void> doDailyUpload(String datastream, LocalDate date) {
        String externalDatasreamUrlJSON = externalConnectorUrl +
                "/datastream/" + datastream +
                "?year=" + date.getYear() +
                "&month=" + date.getMonthValue() +
                "&day=" + date.getDayOfMonth() +
                "&format=json";

        return doUpload(externalDatasreamUrlJSON, datastream, date);
    }

    public Future<Void> doMonthlyUpload(String datastream, LocalDate date) {
        String externalDatasreamUrlJSON = externalConnectorUrl +
                "/datastream/" + datastream +
                "?year=" + date.getYear() +
                "&month=" + date.getMonthValue() +
                "&format=json";
        return doUpload(externalDatasreamUrlJSON, datastream, date);
    }

    private Future<Void> doUpload(String externalDatastreamUrl, String datastream, LocalDate date) {
        return Future.future(upload -> {
            String datasetId = getDatasetId(datastream, date);
            String datasetUrl = buildDatasetURL(datasetId);

            // metadata check cannot fail
            metadataExistsInHub(datasetUrl).onSuccess(datasetExists -> {
                if (datasetExists) {
                    log.debug("Dataset [" + datasetUrl + "] already exists, checking if distribution exists...");

                    distributionExists(datasetUrl, externalDatastreamUrl).onSuccess(distributionExists -> {
                        if (distributionExists) {
                            log.debug("Distribution [" + externalDatastreamUrl + "] already exists, skipping upload");
                            upload.complete();
                        } else {
                            String distributionUrl = buildDistributionURL(datasetId);
                            postDistribution(substituteDistribution(datastream, externalDatastreamUrl, date), distributionUrl, upload);
                            log.info("Distribution uploaded: " + externalDatastreamUrl);
                        }
                    });
                } else {
                    putDataset(substituteDataset(datastream, externalDatastreamUrl, date), datasetUrl, upload);
                    log.info("Dataset uploaded: " + externalDatastreamUrl);
                }
            });
        });
    }

    private String substituteDataset(String datastream, String datastreamUrl, LocalDate date) {
        return substituteTemplate("dataset", datastream, datastreamUrl, date);
    }

    private String substituteDistribution(String datastream, String datastreamUrl, LocalDate date) {
        return substituteTemplate("distribution", datastream, datastreamUrl, date);
    }

    private String substituteTemplate(String jsonPath, String datastream, String datastreamUrl, LocalDate date) {
        Map<String, String> replacements = uploadSettings.getJsonObject(datastream)
                .getJsonObject("replacements", new JsonObject()).stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> (String) entry.getValue()));

        if(datastreamUrl.toLowerCase().matches(".*&day=.*")){
            replacements.putIfAbsent("distributionDate", "den " + date.getDayOfMonth() + "." + date.getMonthValue()+ "." + date.getYear());
        }else{
            replacements.putIfAbsent("distributionDate",  "den gesamten Monat "  + date.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN) + " " + date.getYear());
        }

        if(datastreamUrl.toLowerCase().contains("format=csv")){
            replacements.putIfAbsent("distributionDescription", "Strukturierte Darstellung als CSV");
        }else{
            replacements.putIfAbsent("distributionDescription", "Strukturierte Darstellung als JSON");
        }

        replacements.putIfAbsent("datasetDate",  date.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMAN) + " " + date.getYear());
        replacements.putIfAbsent("timestamp", ZonedDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));

        replacements.putIfAbsent("datasetId", UUID.randomUUID().toString());
        replacements.putIfAbsent("distributionId", UUID.randomUUID().toString());
        replacements.putIfAbsent("datastream", datastream);
        replacements.putIfAbsent("datastreamUrl", datastreamUrl);

        StringSubstitutor substitutor = new StringSubstitutor(replacements);
        return substitutor.replace(uploadSettings.getJsonObject(datastream).getJsonObject(jsonPath).getString("template"));
    }

    public Future<List<String>> getAllDatastreams() {
        return getAllDataStreams(1);
    }

    private Future<List<String>> getAllDataStreams(int fromPage) {
        return Future.future(allDataStreams ->
                getDataStreamPage(fromPage)
                        .flatMap(list ->
                                list.isEmpty()
                                        ? Future.succeededFuture(list)
                                        : getAllDataStreams(fromPage + 1).map(list2 -> {
                                    list2.addAll(list);
                                    return list2;
                                })
                        )
                        .setHandler(allDataStreams));
    }

    private Future<List<String>> getDataStreamPage(int pageNumber) {
        return Future.future(result -> {
            Promise<HttpResponse<Buffer>> datastreamPage = Promise.promise();
            circuitBreaker.execute(promise ->
                            webClient.getAbs(connectorUrl + "/datastream?pageSize=100" + "&p=" + pageNumber)
                                    .send(promise)
                    , datastreamPage);

            datastreamPage
                    .future()
                    .map(HttpResponse::bodyAsJsonObject)
                    .map(jsonObject -> jsonObject.getJsonArray("datasetLocations", new JsonArray()).stream()
                            .map(s -> (String) s)
                            .map(s -> s.substring(s.lastIndexOf("/") + 1))
                            .collect(Collectors.toList()))
                    .onSuccess(result::complete)
                    .onFailure(result::fail);
        });
    }

    public Future<Boolean> monthlyWindowExistsInDataStore(String datastream, LocalDate date) {
        return Future.future(result -> {
            int year = date.getYear();
            int month = date.getMonthValue();
            webClient.headAbs(connectorUrl + "/datastream/" + datastream)
                    .addQueryParam("year", String.valueOf(year))
                    .addQueryParam("month", String.valueOf(month))
                    .expect(ResponsePredicate.SC_OK)
                    .send(ar -> result.complete(ar.succeeded()));
        });
    }

    public Future<Boolean> dailyWindowExistsInDataStore(String datastream, LocalDate date) {
        return Future.future(windowExists -> {
            int year = date.getYear();
            int month = date.getMonthValue();
            int day = date.getDayOfMonth();
            webClient.headAbs(connectorUrl + "/datastream/" + datastream)
                    .addQueryParam("year", String.valueOf(year))
                    .addQueryParam("month", String.valueOf(month))
                    .addQueryParam("day", String.valueOf(day))
                    .expect(ResponsePredicate.SC_OK)
                    .send(ar -> windowExists.complete(ar.succeeded()));
        });
    }

    public Future<Boolean> metadataExistsInHub(String url) {
        return Future.future(datasetExists -> {
            webClient.getAbs(url)
                    .putHeader("Accept", "text/turtle")
                    .expect(ResponsePredicate.SC_OK)
                    .send(ar -> datasetExists.complete(ar.succeeded()));
        });
    }

    private Future<JsonObject> getDataset(String datasetUrl){
        Promise<JsonObject> result = Promise.promise();
        webClient.getAbs(datasetUrl)
                .putHeader("Accept", "application/ld+json")
                .expect(ResponsePredicate.SC_OK)
                .as(BodyCodec.jsonObject())
                .send(ar -> {
                    if(ar.succeeded()){
                        result.complete(ar.result().body());
                    }else{
                        result.fail(ar.cause().getMessage());
                    }
                });
        return result.future();
    }

    private Future<Boolean> distributionExists(String datasetUrl,String datastreamUrl){
        Promise<Boolean> exists = Promise.promise();
        getDataset(datasetUrl)
                .onSuccess(json -> exists.complete(distributionExistsInJson(json, datastreamUrl))
                )
                .onFailure(error ->exists.complete(false)
                );

        return exists.future();
    }

    private Boolean distributionExistsInJson(JsonObject json, String datastreamUrl){
        JsonArray graph = json.getJsonArray("@graph", new JsonArray());
        int graphSize = graph.size();
        for(int i=0; i<graphSize; i++){
            String accessUrl = graph.getJsonObject(i).getString("accessURL", null);
            if(accessUrl != null && accessUrl.equals(datastreamUrl)) {
                return true;
            }
        }
        return false;
    }
    private void putDataset(String dataset, String url, Promise<Void> putDataset) {
        webClient.putAbs(url)
                .putHeader("Authorization", this.hubAPIKey)
                .putHeader("Content-Type", "text/turtle")
                .expect(ResponsePredicate.create(response ->
                        response.statusCode() == 200 || response.statusCode() == 201
                                ? ResponsePredicateResult.success()
                                : ResponsePredicateResult.failure("Response status code " + response.statusCode() + " is not 200 or 201")))
                .sendBuffer(Buffer.buffer(dataset), ar -> {
                    if (ar.succeeded()) {
                        putDataset.handle(Future.succeededFuture());
                    } else {
                        putDataset.handle(Future.failedFuture(ar.cause()));
                    }
                });
    }

    private void postDistribution(String distribution, String url, Promise<Void> postDistribution) {
        webClient.postAbs(url)
                .putHeader("Authorization", this.hubAPIKey)
                .putHeader("Content-Type", "text/turtle")
                .expect(ResponsePredicate.SC_CREATED)
                .sendBuffer(Buffer.buffer(distribution), ar -> {
                    if (ar.succeeded()) {

                        postDistribution.handle(Future.succeededFuture());
                    } else {
                        postDistribution.handle(Future.failedFuture(ar.cause()));
                    }
                });
    }

    private String buildDatasetURL(String id) {
        return this.hubUrl + "/datasets/" + id + "?catalogue=quarz";
    }

    private String buildDistributionURL(String datasetId) {
        return this.hubUrl + "/distributions/?dataset=" + datasetId + "&catalogue=quarz";
    }

    private String getDatasetId(String datastream, LocalDate date) {
        return datastream.replace(".", "-").toLowerCase() + "-" + date.getMonthValue() + "-" + date.getYear();
    }

    @Override
    public void close(Handler<AsyncResult<Void>> handler) {
        webClient.close();
        circuitBreaker.close();
        handler.handle(Future.succeededFuture());
    }
}
