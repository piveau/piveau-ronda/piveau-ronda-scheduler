package io.piveau.DataStore.Scheduler.util;

import com.google.common.collect.Sets;
import io.piveau.DataStore.Scheduler.MainVerticle;
import io.piveau.json.ConfigHelper;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import org.apache.log4j.Logger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.reflections.Reflections;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.quartz.CronScheduleBuilder.cronSchedule;

/**
 * The SchedulerUtils class contains the functionality for starting a Quartz Scheduler in Vertx
 */
public class SchedulerUtils {
    private static final Logger log = Logger.getLogger(SchedulerUtils.class);

    private final Vertx vertx;
    private final JsonObject config;
    private Scheduler scheduler;

    public SchedulerUtils(Vertx vertx, JsonObject config, Handler<AsyncResult<SchedulerUtils>> resultHandler) {
        this.vertx = vertx;
        this.config = config;

        vertx.executeBlocking(promise -> {
            try {
                scheduler = new StdSchedulerFactory().getScheduler();
                promise.complete(this);
            } catch (SchedulerException e) {
                promise.fail(e);
            }
        }, resultHandler);
    }

    public Future<Void> startScheduler() {
        return Future.future(startScheduler ->
                vertx.executeBlocking(promise -> {
                    try {
                        scheduler.start();
                        promise.complete();
                    } catch (SchedulerException e) {
                        promise.fail(e);
                    }
                }, startScheduler));
    }

    public Future<Void> stopScheduler(boolean finishJobs) {
        return Future.future(stopScheduler ->
                vertx.executeBlocking(promise -> {
                    try {
                        scheduler.shutdown(finishJobs);
                        promise.complete();
                    } catch (SchedulerException e) {
                        promise.fail(e);
                    }
                }, stopScheduler));
    }

    public Future<Void> scheduleJobs() {
        return Future.future(scheduleJobs -> {
            List<Future> scheduleFutures = allJobs()
                    .stream()
                    .filter(jobClass -> ConfigHelper.forConfig(config).forceJsonObject(ApplicationConfig.ENV_SCHEDULE)
                            .containsKey(jobClass.getSimpleName())
                    )
                    .map(this::scheduleJob)
                    .collect(Collectors.toList());

            CompositeFuture.all(scheduleFutures)
                    .onSuccess(success -> scheduleJobs.complete())
                    .onFailure(scheduleJobs::fail);
        });
    }

    public Future<Void> scheduleJob(Class<? extends Job> job) {
        log.info("Scheduling " + job.getName());

        return Future.future(scheduleJob ->
                vertx.executeBlocking(promise -> {
                    JsonObject jobData = ConfigHelper.forConfig(config).forceJsonObject(job.getSimpleName());
                    String jobCron = ConfigHelper.forConfig(config).forceJsonObject(ApplicationConfig.ENV_SCHEDULE).getString(job.getSimpleName());
                    JobDetail jobDetail = JobBuilder.newJob(job)
                            .withIdentity(job.getSimpleName(), job.getPackage().getName())
                            .usingJobData(new JobDataMap(jobData.getMap()))
                            .build();
                    CronTrigger trigger = TriggerBuilder.newTrigger()
                            .withIdentity(job.getSimpleName(), job.getPackage().getName())
                            .withSchedule(cronSchedule(jobCron))
                            .build();

                    try {
                        scheduler.scheduleJob(jobDetail, Sets.newHashSet(trigger), true);
                        promise.complete();
                    } catch (SchedulerException e) {
                        promise.fail(e);
                    }
                }, scheduleJob));
    }

    private Set<Class<? extends Job>> allJobs() {
        Reflections reflections = new Reflections(MainVerticle.class.getPackage().getName());
        return reflections.getSubTypesOf(Job.class);
    }
}
