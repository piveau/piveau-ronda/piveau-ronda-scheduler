package io.piveau.DataStore.Scheduler.verticles;

import io.piveau.DataStore.Scheduler.util.SchedulerUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import org.apache.log4j.Logger;

/**
 * The SchedulerVerticle contains the functionality to start a Quartz scheduler based on the SchedulerUtils class
 */
public class SchedulerVerticle extends AbstractVerticle {

    private static final Logger log = Logger.getLogger(SchedulerVerticle.class);
    private SchedulerUtils schedulerUtils;

    @Override
    public void start(Promise<Void> startPromise) {
        Promise<SchedulerUtils> schedulerCreated = Promise.promise();
        this.schedulerUtils = new SchedulerUtils(vertx, config(), schedulerCreated);
        schedulerCreated.future()
                .flatMap(aVoid -> schedulerUtils.scheduleJobs())
                .flatMap(aVoid -> schedulerUtils.startScheduler())
                .onFailure(startPromise::fail)
                .onSuccess(startPromise::complete);

    }

    public void stop(Promise<Void> stopPromise) {
        schedulerUtils.stopScheduler(true)
                .onFailure(log::error)
                .setHandler(stopPromise);
    }
}
